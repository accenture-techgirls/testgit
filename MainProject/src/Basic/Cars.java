package Basic;

import java.util.*;

public class Cars {

	public static String CarBrand;
	public static String CarModel;
	public static double CarPrice;
	public static int CarConstructionYear;
	public static boolean winterTires;
	
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Enter Car brand");
		Cars.CarBrand = myScanner.nextLine();
		
		System.out.println("Enter Car model");
		Cars.CarModel = myScanner.nextLine();
		
		System.out.println("Enter price");
		Cars.CarPrice = Double.parseDouble(myScanner.nextLine());
		
		System.out.println("Enter Construction Year");
		Cars.CarConstructionYear = Integer.parseInt(myScanner.nextLine());
		
		System.out.println("Enter Does car has winter tires");
		Cars.winterTires = Boolean.parseBoolean(myScanner.nextLine());
		myScanner.close();
		show();
		
	}
	
	public static void show() {
		System.out.println("Car brand: " + Cars.CarBrand);
		System.out.println("Car model: " + Cars.CarModel);
		System.out.println("price: " + Cars.CarPrice);
		System.out.println("Construction Year: " + Cars.CarConstructionYear);
		System.out.println("Does car has winter tires: " + Cars.winterTires);
	}
}
